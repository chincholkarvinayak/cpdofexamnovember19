package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testCompareInt(){

        int k= new MinMax().compareInt(4,3);
        assertEquals("Add", 4, k);

    }
   @Test
    public void testCompareScenario2() throws Exception {

        int k= new MinMax().compareInt(3,4);
        assertEquals("Add", 4, k);

    }
}
